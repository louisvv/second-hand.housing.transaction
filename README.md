## 使用Python Scapy爬取二手房交易信息
##### 我的博客：www.louisvv.com
##### 项目详细描述请查看：[http://www.louisvv.com/?p=1249](http://www.louisvv.com/?p=1249 "详情")

<h3 id="1">必要条件</h3>
- Python 2.7 / Ptyhon 2.7以上
- 安装Scarpy 、MySQLdb
- Mysql 5.x

<h3 id="2">项目说明</h3>
本项目基于Python Scrapy爬虫框架对lianjia房产交易网站二手房小区、小区在售房屋数据进行爬取。

数据爬取为页面递归爬取，整个爬取流程如下：

1. 搜索小区名，在结果页面中寻找小区链接地址，小区链接为第一层链接
2. 根据第一层链接，进入小区描述页面，爬取小区基本信息
3. 爬取基本信息后，在页面中寻找小区在售二手房链接地址，作为第二层链接
4. 根据第二层链接，进入小区在售二手房页面，对在售二手房信息进行爬取
5. 将爬取的数据按照指定输出顺序、分隔符保存到CSV文件中
6. 使用数据解析文件对爬取数据文件进行解析，将保存到Mysql数据库中

<h3 id="3">用法</h3>
1. 安装Scarpy
<br>`pip install Scrapy`
2. 安装MySQLdb
<br>`pip install MySQLdb`
<br>如果你是python 3.x,则使用PyMySQL
<br>
`pip install PyMySQL`
3. 下载此项目
4. 执行sql文件init.sql，初始化数据库，创建表
5. 修改/lianjia/lianjia/data.py 数据解析程序，修改mysql配置
6. 修改/lianjia/lianjia/house_spider.py 数据爬取程序
<br>修改：start_urls 网页地址，不同地区前缀不同
<br>修改：xiaoqu ，小区名   注：start_urls、xiaoqu可多个
7. 修改lj.sh脚本内容：
<br>`修改spider_dir，此目录为项目spiders文件夹地址`>
<br>`修改data_dir，此目录为爬取数据保存目录地址`
7. 赋予脚本执行权限：
<br>`chmod +x ./lj.sh`</br>
8. 创建定时任务，根据实际情况修改
<br>
    `crontab -e`
</br>
    `00 19 * * * sh 目录/lj.sh`
