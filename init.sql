CREATE DATABASE lianjia;
USE lianjia;

SET FOREIGN_KEY_CHECKS=0;


-- ----------------------------
-- Table structure for house_info
-- ----------------------------
DROP TABLE IF EXISTS `house_info`;
CREATE TABLE `house_info` (
  `id` bigint(255) NOT NULL DEFAULT '0',
  `xiaoqu` varchar(255) DEFAULT NULL,
  `pingmi` float(255,2) DEFAULT NULL COMMENT '平米',
  `unitprice` int(20) DEFAULT NULL COMMENT '元/平',
  `price` float(255,1) DEFAULT NULL COMMENT '万元',
  `louceng` varchar(255) DEFAULT NULL,
  `geju` varchar(255) DEFAULT NULL,
  `chaoxiang` varchar(255) DEFAULT NULL,
  `zhuangxiu` varchar(255) DEFAULT NULL,
  `dianti` varchar(255) DEFAULT NULL,
  `guanzhu` varchar(255) DEFAULT NULL,
  `daikan` varchar(255) DEFAULT NULL,
  `fabu` varchar(255) DEFAULT NULL,
  `louxing` varchar(255) DEFAULT NULL,
  `web` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for xiaoqu_info
-- ----------------------------
DROP TABLE IF EXISTS `xiaoqu_info`;
CREATE TABLE `xiaoqu_info` (
  `xiaoqu` varchar(255) NOT NULL,
  `dizhi` varchar(255) DEFAULT NULL,
  `xiaoquPrice` int(11) DEFAULT NULL,
  `xiaoquUnitPriceDesc` varchar(255) NOT NULL,
  `niandai` varchar(255) DEFAULT NULL,
  `wuyefeiyong` varchar(255) DEFAULT NULL,
  `kaifashang` varchar(255) DEFAULT NULL,
  `wuye` varchar(255) DEFAULT NULL,
  `fangwuzongshu` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `loushu` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`xiaoquUnitPriceDesc`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
