﻿# -*- coding: utf-8 -*-
#   作者：louisvv
import MySQLdb
#pymysql.install_as_MySQLdb
import datetime
import time
import sys
def parse_data(path):
    #   mysql数据库配置
    connection = MySQLdb.connect(user='root', passwd='1234', host='localhost', port=3306, charset="utf8")
    cursor = connection.cursor()
    #   选择数据库
    connection.select_db('lianjia')
    file = open(path)
    for line in file.readlines():
        if not line.startswith("houseinfo"):
            data = line.strip().split("^")
            # id housecode主键
            id = int(data[1])
            # web地址
            web = data[2]
            # 小区
            xiaoqu = data[5].strip()
            changdu = data.__len__()
            # 楼房信息
            if (not data[0].__contains__("车位")) & (data[0].endswith("电梯")):
                # if data[0].endswith("电梯"):
                houseinfo = data[0].split("|")
                #   格局
                geju = houseinfo[1].strip()
                #   平米
                pingmi = houseinfo[2].strip().replace("平米", "")
                pingmi = float(pingmi)
                #   房屋朝向
                chaoxiang = houseinfo[3].strip()
                #   装修情况
                zhuangxiu = houseinfo[4].strip()
                #   电梯
                dianti = houseinfo[5].strip()

                # 发布关注数
                follow = data[4].strip().replace("-", "")
                followinfo = follow.split("/")
                #   关注
                guanzhu = followinfo[0].strip()
                #   带看
                daikan = followinfo[1].strip()
                #   发布时间
                fabu = followinfo[2].strip()
                # 楼层，小区
                flood = data[3].replace("-", "").strip().split(")")
                flood_len = int(flood.__len__())
                if flood[0].endswith("层"):
                    louceng = flood[0] + ")"

                # 单价
                unitprice = int(data[6].replace("单价", "").replace("元/平米", ""))
                # 价格
                price = float(data[7])
                # 小区地址
                dizhi = data[8]
                # 均价
                xiaoquPrice = int(data[9])
                # 均价描述
                xiaoquUnitPriceDesc = data[10]
                # 建成年份
                niandai = data[11]
                # 楼型
                louxing = data[12]
                # 物业费用
                wuyefeiyong = data[13]
                # 物业
                wuye = data[14]
                # 楼数
                loushu = data[15]
                # 房屋总数
                fangwuzongshu = data[16]
                # 开发商
                kaifashang = data[17]
                #   date字段标准化
                date = datetime.datetime.now().strftime('%Y-%m-%d')
                #   将house_info的数据添加到元组中
                tmp = (id, xiaoqu, pingmi, unitprice, price, louceng, geju, chaoxiang, zhuangxiu, dianti, guanzhu, daikan,
                       fabu, louxing, web, date)
                #   将元组中的数据插入house_info表中，无视主键存在插入异常
                sql = "insert ignore into house_info(id, xiaoqu, pingmi, unitprice, price, louceng, geju, chaoxiang, zhuangxiu, dianti, guanzhu, daikan,fabu,louxing,web,date) \
                values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                #   执行sql语句
                cursor.execute(sql, tmp)
                year = datetime.datetime.now().year
                #   xiaoqu_info表的主键 小区名-年份-小区该月份单价
                xiaoquUnitPriceDesc_pri= xiaoqu+"-"+str(year)+"-"+xiaoquUnitPriceDesc
                #   将xiaoqu_info的数据保存到元组中
                xiaoqu_tmp=(xiaoqu, dizhi, xiaoquPrice, xiaoquUnitPriceDesc_pri, niandai, wuyefeiyong, wuye, loushu, fangwuzongshu,kaifashang, date,)
                #   将元组中的数据插入xiaoqu_info表中，无视主键存在插入异常
                xiaoqu_sql = "insert ignore into xiaoqu_info(xiaoqu,dizhi,xiaoquPrice,xiaoquUnitPriceDesc,niandai,wuyefeiyong,wuye,loushu,fangwuzongshu,kaifashang,date) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                #   执行sql语句
                cursor.execute(xiaoqu_sql, xiaoqu_tmp)
    cursor.close()
    connection.commit()
    connection.close()

if __name__ == '__main__':
    #   获取命令行传入参数，参数为数据文件保存地址
    path=sys.argv[1]
    #   将地址传入，解析数据，并导入mysql数据库
    parse_data(path)