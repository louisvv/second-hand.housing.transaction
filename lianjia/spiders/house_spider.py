# -*- coding: utf-8 -*-
import scrapy
from lianjia.items import LianjiaItem

class House_spider(scrapy.Spider):
    #   程序名称  （唯一）
    name = 'house_spider'
    #   输入所要查找的小区名
    xiaoqu= '星海人家'
    xiaoqu2='小区2'
    #   注意:这里的地址为大连地区小区，可根据实际情况进行替换
    start_urls =[
                  'https://dl.lianjia.com/xiaoqu/rs%s/' %xiaoqu,
                  'https://dl.lianjia.com/xiaoqu/rs%s/' % xiaoqu2
                  ]

    #   首先解析搜索到小区的页面
    def parse(self, response):
        #   找到该超链接
        for href in response.xpath('.//div[@class="title"]/a/@href'):
            #   添加该链接到url
            url = response.urljoin(href.extract())
            yield scrapy.Request(url, callback=self.parse_xiaoqu)

    #   解析小区信息
    def parse_xiaoqu(self, response):
        item = LianjiaItem()
        #   获取小区地址
        item['dizhi'] = response.xpath('.//div[@class="detailDesc"]/text()').extract()
        #   获取小区单价
        item['xiaoquPrice'] = response.xpath('.//span[@class="xiaoquUnitPrice"]/text()').extract()
        #   获取小区单价描述信息
        item['xiaoquUnitPriceDesc'] = response.xpath('.//span[@class="xiaoquUnitPriceDesc"]/text()').extract()
        #   获取小区年代
        item['niandai'] = response.xpath('/html/body/div[6]/div[2]/div[2]/div[1]/span[2]/text()').extract()
        #   获取小区楼型
        item['louxing'] = response.xpath('/html/body/div[6]/div[2]/div[2]/div[2]/span[2]/text()').extract()
        #   获取小区物业费用
        item['wuyefeiyong'] = response.xpath('/html/body/div[6]/div[2]/div[2]/div[3]/span[2]/text()').extract()
        #   获取小区物业
        item['wuye'] = response.xpath('/html/body/div[6]/div[2]/div[2]/div[4]/span[2]/text()').extract()
        #   获取小区楼栋总数
        item['loushu'] = response.xpath('/html/body/div[6]/div[2]/div[2]/div[6]/span[2]/text()').extract()
        #   获取小区去房屋总数
        item['fangwuzongshu'] = response.xpath('/html/body/div[6]/div[2]/div[2]/div[7]/span[2]/text()').extract()
        #   获取小区开发商
        item['kaifashang'] = response.xpath('/html/body/div[6]/div[2]/div[2]/div[5]/span[2]/text()').extract()

        #   点击链接，查看小区全部在售二手房
        for href in response.xpath('//*[@id="goodSell"]/div/a/@href'):
            for i in range(1,2):
                url = response.urljoin(href.extract())
                url1=url+"pg"+format(i)
                yield scrapy.Request(url1, meta={'item': item}, callback=self.parse_house)

    #   解析在售二手房数据
    def parse_house(self, response):
        for house in response.xpath('.//ul[@class="sellListContent"]/li[@class="clear"]'):
            item = response.meta['item']
            #   获取小区地址
            item['region'] = house.xpath('.//div[@class="houseInfo"]/a/text()').extract()
            #   获取房屋信息
            item['houseinfo'] = house.xpath('.//div[@class="houseInfo"]/text()').extract()
            #   获取层数
            item['flood'] = house.xpath('.//div[@class="positionInfo"]/text()').extract()
            #   获取房屋总价格
            item['price'] = house.xpath('.//div[@class="totalPrice"]/span/text()').extract()
            #   获取房屋每平米价格
            item['unitprice'] = house.xpath('.//div[@class="unitPrice"]/span/text()').extract()
            #   获取关注人数
            item['follow'] = house.xpath('.//div[@class="followInfo"]/text()').extract()
            #   获取图片链接
            item['img'] = house.xpath('.//a[@class="img "]/@href').extract()
            #   获取house code id
            item['housecode'] = house.xpath('.//a[@class="img "]/@data-housecode').extract()
            yield item
