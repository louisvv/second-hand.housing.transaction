# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class LianjiaItem(scrapy.Item):
    # define the fields for your item here like:
    houseinfo =scrapy.Field()
    housecode =scrapy.Field()
    img=scrapy.Field()
    flood=scrapy.Field()
    follow=scrapy.Field()
    region=scrapy.Field()
    unitprice=scrapy.Field()
    price=scrapy.Field()
    dizhi=scrapy.Field()
    xiaoquPrice=scrapy.Field()
    xiaoquUnitPriceDesc=scrapy.Field()
    niandai=scrapy.Field()
    louxing=scrapy.Field()
    wuyefeiyong=scrapy.Field()
    wuye=scrapy.Field()
    loushu=scrapy.Field()
    fangwuzongshu=scrapy.Field()
    kaifashang=scrapy.Field()